import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutusComponent } from '../components/website/aboutus/aboutus.component';
import { HeroComponent } from '../components/website/hero/hero.component';
import { PaymentComponent } from '../components/website/payment/payment.component';
import { TrendComponent } from '../components/website/trend/trend.component';
import { ServicesComponent } from '../components/website/services/services.component';
import { FooterComponent } from '../components/website/footer/footer.component';
import { LandingpageComponent } from '../components/website/landingpage/landingpage.component';
import { AppRoutingModule } from '../app-routing.module';
import { HomeComponent } from '../components/website/home/home.component';



const websitemodule = [
  AboutusComponent,
  HeroComponent,
  PaymentComponent,
  TrendComponent,
  ServicesComponent,
  HomeComponent,
  FooterComponent,
  LandingpageComponent
]


@NgModule({
  declarations: [
    websitemodule,
  
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    websitemodule
  ]
})
export class WebsiteModule { }
