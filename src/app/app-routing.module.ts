import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FooterComponent } from './components/website/footer/footer.component';
import { HeroComponent } from './components/website/hero/hero.component';
import { LandingpageComponent } from './components/website/landingpage/landingpage.component';

const routes: Routes = [
  {path: "", component: HeroComponent},
  {path: "landing", component: LandingpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
